﻿
namespace holamundoform
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnok = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelbienvenido = new System.Windows.Forms.Label();
            this.btnclose = new System.Windows.Forms.Button();
            this.textfecha = new System.Windows.Forms.TextBox();
            this.textcodigo = new System.Windows.Forms.TextBox();
            this.textnombre = new System.Windows.Forms.TextBox();
            this.textapellido = new System.Windows.Forms.TextBox();
            this.textdireccion = new System.Windows.Forms.TextBox();
            this.texttelefono = new System.Windows.Forms.TextBox();
            this.textsalario = new System.Windows.Forms.TextBox();
            this.textnomina = new System.Windows.Forms.TextBox();
            this.btnimprimir = new System.Windows.Forms.Button();
            this.btnlimpiar = new System.Windows.Forms.Button();
            this.cbox1 = new System.Windows.Forms.ComboBox();
            this.cbox2 = new System.Windows.Forms.ComboBox();
            this.cbox3 = new System.Windows.Forms.ComboBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.apellidos = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contrato = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cargo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.departamento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.salario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 316);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "dias trabajados";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnok
            // 
            this.btnok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnok.Location = new System.Drawing.Point(482, 76);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(96, 36);
            this.btnok.TabIndex = 1;
            this.btnok.Text = "registrar";
            this.btnok.UseVisualStyleBackColor = false;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "codigo de empleado";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "nombre";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "apellidos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 15);
            this.label5.TabIndex = 5;
            this.label5.Text = "direccion";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 15);
            this.label6.TabIndex = 6;
            this.label6.Text = "telefonos";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 15);
            this.label7.TabIndex = 7;
            this.label7.Text = "condicion";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(115, 15);
            this.label8.TabIndex = 8;
            this.label8.Text = "cargo desempeñado";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 236);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 9;
            this.label9.Text = "departamento";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 267);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 15);
            this.label10.TabIndex = 10;
            this.label10.Text = "salario actual";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(12, 292);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(148, 15);
            this.label11.TabIndex = 11;
            this.label11.Text = "numero de cuenta nomina";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::holamundoform.Properties.Resources.ripndip;
            this.pictureBox1.Location = new System.Drawing.Point(617, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(197, 169);
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // labelbienvenido
            // 
            this.labelbienvenido.AutoSize = true;
            this.labelbienvenido.Location = new System.Drawing.Point(482, 313);
            this.labelbienvenido.Name = "labelbienvenido";
            this.labelbienvenido.Size = new System.Drawing.Size(66, 15);
            this.labelbienvenido.TabIndex = 14;
            this.labelbienvenido.Text = "bienvenido";
            this.labelbienvenido.Click += new System.EventHandler(this.labelbienvenido_Click);
            // 
            // btnclose
            // 
            this.btnclose.BackColor = System.Drawing.Color.Red;
            this.btnclose.Location = new System.Drawing.Point(753, 316);
            this.btnclose.Name = "btnclose";
            this.btnclose.Size = new System.Drawing.Size(75, 23);
            this.btnclose.TabIndex = 15;
            this.btnclose.Text = "cerrar";
            this.btnclose.UseVisualStyleBackColor = false;
            this.btnclose.Click += new System.EventHandler(this.btnclose_Click);
            // 
            // textfecha
            // 
            this.textfecha.Location = new System.Drawing.Point(160, 313);
            this.textfecha.Name = "textfecha";
            this.textfecha.Size = new System.Drawing.Size(100, 23);
            this.textfecha.TabIndex = 16;
            this.textfecha.TextChanged += new System.EventHandler(this.textfecha_TextChanged);
            // 
            // textcodigo
            // 
            this.textcodigo.Location = new System.Drawing.Point(160, 29);
            this.textcodigo.Name = "textcodigo";
            this.textcodigo.Size = new System.Drawing.Size(100, 23);
            this.textcodigo.TabIndex = 16;
            this.textcodigo.TextChanged += new System.EventHandler(this.textcodigo_TextChanged);
            // 
            // textnombre
            // 
            this.textnombre.Location = new System.Drawing.Point(160, 57);
            this.textnombre.Name = "textnombre";
            this.textnombre.Size = new System.Drawing.Size(100, 23);
            this.textnombre.TabIndex = 16;
            this.textnombre.TextChanged += new System.EventHandler(this.textnombre_TextChanged);
            // 
            // textapellido
            // 
            this.textapellido.Location = new System.Drawing.Point(160, 84);
            this.textapellido.Name = "textapellido";
            this.textapellido.Size = new System.Drawing.Size(100, 23);
            this.textapellido.TabIndex = 16;
            this.textapellido.TextChanged += new System.EventHandler(this.textapellido_TextChanged);
            // 
            // textdireccion
            // 
            this.textdireccion.Location = new System.Drawing.Point(160, 113);
            this.textdireccion.Name = "textdireccion";
            this.textdireccion.Size = new System.Drawing.Size(100, 23);
            this.textdireccion.TabIndex = 16;
            this.textdireccion.TextChanged += new System.EventHandler(this.textdireccion_TextChanged);
            // 
            // texttelefono
            // 
            this.texttelefono.Location = new System.Drawing.Point(160, 142);
            this.texttelefono.Name = "texttelefono";
            this.texttelefono.Size = new System.Drawing.Size(100, 23);
            this.texttelefono.TabIndex = 16;
            this.texttelefono.TextChanged += new System.EventHandler(this.texttelefono_TextChanged);
            // 
            // textsalario
            // 
            this.textsalario.Location = new System.Drawing.Point(160, 256);
            this.textsalario.Name = "textsalario";
            this.textsalario.Size = new System.Drawing.Size(100, 23);
            this.textsalario.TabIndex = 16;
            this.textsalario.TextChanged += new System.EventHandler(this.textsalario_TextChanged);
            // 
            // textnomina
            // 
            this.textnomina.Location = new System.Drawing.Point(160, 284);
            this.textnomina.Name = "textnomina";
            this.textnomina.Size = new System.Drawing.Size(100, 23);
            this.textnomina.TabIndex = 16;
            this.textnomina.TextChanged += new System.EventHandler(this.textnomina_TextChanged);
            // 
            // btnimprimir
            // 
            this.btnimprimir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnimprimir.Location = new System.Drawing.Point(482, 139);
            this.btnimprimir.Name = "btnimprimir";
            this.btnimprimir.Size = new System.Drawing.Size(95, 36);
            this.btnimprimir.TabIndex = 17;
            this.btnimprimir.Text = "imprimir";
            this.btnimprimir.UseVisualStyleBackColor = false;
            this.btnimprimir.Click += new System.EventHandler(this.btnimprimir_Click);
            // 
            // btnlimpiar
            // 
            this.btnlimpiar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btnlimpiar.Location = new System.Drawing.Point(482, 21);
            this.btnlimpiar.Name = "btnlimpiar";
            this.btnlimpiar.Size = new System.Drawing.Size(96, 36);
            this.btnlimpiar.TabIndex = 18;
            this.btnlimpiar.Text = "limpiar";
            this.btnlimpiar.UseVisualStyleBackColor = false;
            this.btnlimpiar.Click += new System.EventHandler(this.btnlimpiar_Click);
            // 
            // cbox1
            // 
            this.cbox1.FormattingEnabled = true;
            this.cbox1.Items.AddRange(new object[] {
            "fijo",
            "contrato",
            "otro"});
            this.cbox1.Location = new System.Drawing.Point(160, 170);
            this.cbox1.Name = "cbox1";
            this.cbox1.Size = new System.Drawing.Size(100, 23);
            this.cbox1.TabIndex = 19;
            // 
            // cbox2
            // 
            this.cbox2.FormattingEnabled = true;
            this.cbox2.Items.AddRange(new object[] {
            "desarrollador 1",
            "desarrollador 2",
            "desarrollador 3",
            "desarrollador 4",
            "gerente"});
            this.cbox2.Location = new System.Drawing.Point(160, 200);
            this.cbox2.Name = "cbox2";
            this.cbox2.Size = new System.Drawing.Size(100, 23);
            this.cbox2.TabIndex = 20;
            // 
            // cbox3
            // 
            this.cbox3.FormattingEnabled = true;
            this.cbox3.Items.AddRange(new object[] {
            "desarrollo",
            "aplicacion",
            "pruebas",
            "otro",
            ""});
            this.cbox3.Location = new System.Drawing.Point(160, 230);
            this.cbox3.Name = "cbox3";
            this.cbox3.Size = new System.Drawing.Size(100, 23);
            this.cbox3.TabIndex = 21;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(614, 227);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 23);
            this.dateTimePicker1.TabIndex = 22;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 359);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(816, 92);
            this.dataGridView1.TabIndex = 23;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.SelectionChanged += new System.EventHandler(this.dataGridView1_SelectionChanged);
            // 
            // codigo
            // 
            this.codigo.HeaderText = "codigo";
            this.codigo.Name = "codigo";
            // 
            // nombre
            // 
            this.nombre.HeaderText = "nombre";
            this.nombre.Name = "nombre";
            // 
            // apellidos
            // 
            this.apellidos.HeaderText = "apellidos";
            this.apellidos.Name = "apellidos";
            // 
            // direccion
            // 
            this.direccion.HeaderText = "direccion";
            this.direccion.Name = "direccion";
            // 
            // telefono
            // 
            this.telefono.HeaderText = "telefono";
            this.telefono.Name = "telefono";
            // 
            // contrato
            // 
            this.contrato.HeaderText = "contrato";
            this.contrato.Name = "contrato";
            // 
            // cargo
            // 
            this.cargo.HeaderText = "cargo";
            this.cargo.Name = "cargo";
            // 
            // departamento
            // 
            this.departamento.HeaderText = "departamento";
            this.departamento.Name = "departamento";
            // 
            // salario
            // 
            this.salario.HeaderText = "salario";
            this.salario.Name = "salario";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.button1.Location = new System.Drawing.Point(482, 200);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(95, 36);
            this.button1.TabIndex = 24;
            this.button1.Text = "borrar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(840, 463);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.cbox3);
            this.Controls.Add(this.cbox2);
            this.Controls.Add(this.cbox1);
            this.Controls.Add(this.btnlimpiar);
            this.Controls.Add(this.btnimprimir);
            this.Controls.Add(this.textnomina);
            this.Controls.Add(this.textsalario);
            this.Controls.Add(this.texttelefono);
            this.Controls.Add(this.textdireccion);
            this.Controls.Add(this.textapellido);
            this.Controls.Add(this.textnombre);
            this.Controls.Add(this.textcodigo);
            this.Controls.Add(this.textfecha);
            this.Controls.Add(this.btnclose);
            this.Controls.Add(this.labelbienvenido);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "tu nomina";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelbienvenido;
        private System.Windows.Forms.Button btnclose;
        private System.Windows.Forms.TextBox textfecha;
        private System.Windows.Forms.TextBox textcodigo;
        private System.Windows.Forms.TextBox textnombre;
        private System.Windows.Forms.TextBox textapellido;
        private System.Windows.Forms.TextBox textdireccion;
        private System.Windows.Forms.TextBox texttelefono;
        private System.Windows.Forms.TextBox textsalario;
        private System.Windows.Forms.TextBox textnomina;
        private System.Windows.Forms.Button btnimprimir;
        private System.Windows.Forms.Button btnlimpiar;
        private System.Windows.Forms.ComboBox cbox1;
        private System.Windows.Forms.ComboBox cbox2;
        private System.Windows.Forms.ComboBox cbox3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn apellidos;
        private System.Windows.Forms.DataGridViewTextBoxColumn direccion;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn contrato;
        private System.Windows.Forms.DataGridViewTextBoxColumn cargo;
        private System.Windows.Forms.DataGridViewTextBoxColumn departamento;
        private System.Windows.Forms.DataGridViewTextBoxColumn salario;
        private System.Windows.Forms.Button button1;
    }
}

