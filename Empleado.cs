﻿using System;
using System.Collections.Generic;
using System.Text;

namespace holamundoform
{
   public class Empleado
    { 
    String fecha;
    String codigo;
    String nombre;
    String apellidos;
    String direccion;
    String telefono;
    String condicion;
    String cargo;
    String departamento;
    double salario;
    String cuenta;
    int dias;

    public Empleado()
    {
        this.fecha = fecha;
        this.codigo = codigo;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.direccion = direccion;
        this.telefono = telefono;
        this.condicion = condicion;
        this.cargo = cargo;
        this.departamento = departamento;
        this.salario = salario;
        this.cuenta = cuenta;
        this.dias = dias;

    }

    public string Fecha { get => fecha; set => fecha = value; }
    public string Codigo { get => codigo; set => codigo = value; }
    public string Nombre { get => nombre; set => nombre = value; }
    public string Apellidos { get => apellidos; set => apellidos = value; }
    public string Direccion { get => direccion; set => direccion = value; }
    public string Telefono { get => telefono; set => telefono = value; }
    public string Condicion { get => condicion; set => condicion = value; }
    public string Cargo { get => cargo; set => cargo = value; }
    public string Departamento { get => departamento; set => departamento = value; }
    public double Salario { get => salario; set => salario = value; }
    public string Cuenta { get => cuenta; set => cuenta = value; }

    public int Dias { get => dias; set => dias = value; }

    public override string ToString()
    {
            return "\n" + "Fecha:" + fecha.ToString() + "\n" +
                    "Código:" + codigo.ToString() + "\n" +
                    "Nombre:" + nombre.ToString() + "\n" +
                    "Apellidos:" + apellidos.ToString() + "\n" +
                    "Dirección:" + direccion.ToString() + "\n" +
                    "Telefono:" + telefono.ToString() + "\n" +
                    "Contrato:" + condicion.ToString() + "\n" +
                    "Cargo:" + cargo.ToString() + "\n" +
                    "Departamento:" + departamento.ToString() + "\n" +
                    "Salario:" + salario.ToString() + "\n" +
                    "Cuenta:" + cuenta.ToString() + "\n" +
                    "Días trabajados:" + dias.ToString() + "\n";
    }

    }
}

