﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace holamundoform
{
    public partial class Form1 : Form
    {
        List<Empleado> lista = new List<Empleado>();
        private DataTable dt;
        public Form1()
        {
            InitializeComponent();
        }

        public void Datos()
        {
            Empleado em = new Empleado();
            lista.Add(em);
            string textodeltexbox = textnombre.Text;
            labelbienvenido.Text = textodeltexbox + " ha sido agregado correctamente";


            em.Fecha = dateTimePicker1.ToString();
            em.Codigo = textcodigo.Text;
            em.Nombre = textnombre.Text;
            em.Apellidos = textapellido.Text;
            em.Direccion = textdireccion.Text;
            em.Telefono = texttelefono.Text;
            em.Condicion = cbox1.Text;
            em.Cargo = cbox2.Text;
            em.Departamento = cbox3.Text;
            em.Salario = double.Parse(textsalario.Text);
            em.Cuenta = textnomina.Text;
            em.Dias = int.Parse(textfecha.Text);
            DataRow row = dt.NewRow();

            row["codigo"] = textcodigo.Text;
            row["nombre"] = textnombre.Text;
            row["apellidos"] = textapellido.Text;
            row["direccion"] = textdireccion.Text;
            row["telefono"] = texttelefono.Text;
            row["condicion"] = cbox1.Text;
            row["cargo"] = cbox2.Text;
            row["departamento"] = cbox3.Text;
            row["salario"] = textsalario.Text;
            row["cuenta"] = textnomina.Text;
            row["dias"] = textfecha.Text;
            dt.Rows.Add(row);

        }

        public void Imprimir()
        {
            StreamWriter outputfile;

            try
            {

                outputfile = new StreamWriter("C:\\Users\\Usuario\\Desktop\\nomina.txt");

                outputfile.WriteLine(String.Join(" ", lista));
                outputfile.Close();

            }
            catch (Exception w)
            {

            }
        }

        public void Eliminar()
        {
            int fila = dataGridView1.CurrentRow.Index;

            if (fila < 0)
            {

            }
            else
            {
                dataGridView1.Rows.RemoveAt(dataGridView1.CurrentRow.Index);
                lista.RemoveAt(fila);
                
            }
            
        }
        private void Form1_Load(object sender, EventArgs e)
        {

            dt = new DataTable();
            dt.Columns.Add("codigo");
            dt.Columns.Add("nombre");
            dt.Columns.Add("apellidos");
            dt.Columns.Add("direccion");
            dt.Columns.Add("telefono");
            dt.Columns.Add("condicion");
            dt.Columns.Add("cargo");
            dt.Columns.Add("departamento");
            dt.Columns.Add("salario");
            dt.Columns.Add("cuenta");
            dt.Columns.Add("dias");
            dataGridView1.DataSource = dt;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

       

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void btnok_Click(object sender, EventArgs e)
        {

            Datos();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void labelbienvenido_Click(object sender, EventArgs e)
        {

        }

        private void textfecha_TextChanged(object sender, EventArgs e)
        {

        }

        private void textcodigo_TextChanged(object sender, EventArgs e)
        {

        }

        private void textnombre_TextChanged(object sender, EventArgs e)
        {

        }

        private void textapellido_TextChanged(object sender, EventArgs e)
        {

        }

        private void textdireccion_TextChanged(object sender, EventArgs e)
        {

        }

        private void texttelefono_TextChanged(object sender, EventArgs e)
        {

        }

        private void textsalario_TextChanged(object sender, EventArgs e)
        {

        }

        private void textnomina_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnimprimir_Click(object sender, EventArgs e)
        {
            Imprimir();
        }

        private void btnlimpiar_Click(object sender, EventArgs e)
        {
            textfecha.Text = "";
            textcodigo.Text = "";
            textnombre.Text = "";
            textapellido.Text="";
            textdireccion.Text = "";
            texttelefono.Text = "";
            textsalario.Text = "";
            textnomina.Text = "";
            cbox1.Text = "";
            cbox2.Text = "";
            cbox3.Text = "";


            

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Eliminar();
            MessageBox.Show("Empleado eliminado");
            Imprimir();

        }

        
        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
    
            var row = (sender as DataGridView).CurrentRow;

            textcodigo.Text = row.Cells[0].Value.ToString();
            textnombre.Text = row.Cells[1].Value.ToString();
            textapellido.Text = row.Cells[2].Value.ToString();
            textdireccion.Text = row.Cells[3].Value.ToString();
            texttelefono.Text = row.Cells[4].Value.ToString();
            cbox1.Text = row.Cells[5].Value.ToString();
            cbox2.Text = row.Cells[6].Value.ToString();
            cbox3.Text = row.Cells[7].Value.ToString();
            textsalario.Text = row.Cells[8].Value.ToString();
            textnomina.Text = row.Cells[9].Value.ToString();
            textfecha.Text = row.Cells[10].Value.ToString();

            
        }
        
    }
}
