﻿using System;
using System.Windows.Forms;

namespace holamundoform
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsr.Text) || string.IsNullOrEmpty(txtPass.Text))
            {
                MessageBox.Show("Favor de llenar todos los campos", "Error");
            }
            else
            {
                if (txtUsr.Text.Equals("Admin") && txtPass.Text.Equals("1234"))
                    {
                        this.Hide();
                        Form1 info = new Form1();
                        info.Show();
                    }
                    else
                    {
                    if (txtUsr.Text.Equals("Empleado") && txtPass.Text.Equals("1234"))
                    {
                        this.Hide();
                        User usuario = new User();
                        usuario.Show();
                    }else
                        MessageBox.Show("Usuario o contraseña incorrectos");
                    }
                }
            }   
            
        

        private void btn2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
    
